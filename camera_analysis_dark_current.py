# @File(label="directory with dark current images", style="directory") source
# @String(label="file type",choices={".vsi"}) filetype
# @Double (label="mean gain value [e-/greyscale]") gain
# @Double (label="readnoise [e-]") read_noise
# @File(label="save results here directory", style="directory") target

# python imports
import sys
import os
import shutil
import glob
import csv
import time
import csv
from itertools import izip
from math import sqrt

# Imagej imports
from ij import IJ
from ij import ImagePlus
from ij import WindowManager as wm
from ij.plugin import ImageCalculator
from loci.plugins import BF

# ome imports to parse metadata
from loci.formats import ImageReader
from loci.formats import MetadataTools
from ome.units import UNITS

# plotting
from jarray import array
from java.awt import Color
from ij.gui import Plot

# fitting
from ij.measure import CurveFitter


def list_dirs_containing_filetype(source, filetype):
    """Recurs through the source dir and return all dirs & subdirs
    that contain the specified filetype

    Arguments:
        source {string} -- Path to source dir
        filetype {string} -- file extension to specify filetype

    Returns:
       stitching_dirs {list} -- list of all dirs that contain filetype
    """

    dirs_containing_filetype = []
    
    # walk recursively through all directories 
    # list their paths and all files inside (=os.walk)
    for dirname, _, filenames in os.walk(source):          
        # stop when encountering a directory that contains "filetype"
        # and store the directory path
        for filename in filenames:
            if filetype in filename:
                dirs_containing_filetype.append(dirname + "/")
                break

    return dirs_containing_filetype


def list_all_filenames(source, filetype):
    """Get a sorted list of all files of specified filetype in a given directory

    Arguments:
        source {string} -- Path to source dir
        filetype {string} -- file extension to specify filetype

    Returns:
       allimages {list} -- list of all files of the given type in the source dir
    """

    os.chdir(str(source))
    allimages = sorted(glob.glob("*"+filetype)) # sorted by name

    return allimages


def calculate_dark_current(source, image1, image2, gain, read_noise):
    """Calculates the camera dark current given two input images, the gain and the read noise

    Arguments:
        source {string} -- Path to source dir
        image1 {string} -- filename of image1 in source dir
        image2 {string} -- filename of image2 in source dir
        gain {float} -- value of the camera gain
        read_noise {float} -- value of the camera read noise

    Returns:
       dark_current {float} -- value of the camera dark current
    """

    path_to_image1 = source + image1
    path_to_image2 = source + image2
    raw_image1 = BF.openImagePlus(str(path_to_image1))
    raw_image2 = BF.openImagePlus(str(path_to_image2))
    # TODO: how do I close the two raw images again?
    
    ic = ImageCalculator()
    difference_image = ic.run("Subtract create 32-bit", raw_image1[0], raw_image2[0])
    difference_image_stats = difference_image.getStatistics()
    stdev_diff_image = difference_image_stats.stdDev
    difference_image.close()

    dark_current = ((stdev_diff_image * gain) / sqrt(2.0)) - read_noise

    return dark_current


def get_exposure_time(source, image_name):
    """Get camera exposure time from ome xml metadata

    Arguments:
        source {string} -- Path to source dir
        image_name {string} -- image filename in source dir

    Returns:
       a tuple that contains:
       exposure_time_value {float} -- value of the camera exposure time
       exposure_time_value {string} -- unit of the exposure time
    """

    path_to_image = source + image_name
    reader = ImageReader()
    omeMeta = MetadataTools.createOMEXMLMetadata()
    reader.setMetadataStore(omeMeta)
    reader.setId(path_to_image)
    exposure_time = omeMeta.getPlaneExposureTime(0,0)
    exposure_time_value = exposure_time.value()
    exposure_time_unit = exposure_time.unit().getSymbol()
    reader.close()
    
    return  exposure_time_value, exposure_time_unit


def save_current_image_as_PNG(target):
    """save the currently active image as ImageJ-PNG
    
    Arguments:
        target {directory} -- directory where the image will be saved

    Returns:
        nothing
    """
    
    imp = wm.getCurrentImage()
    title = imp.getShortTitle()
    savename = title + ".png"
    savepath = str(target) + "/" + savename
    IJ.saveAs(imp, "PNG", savepath)
    print "now saving", savepath 
    imp.close()


def plot_x_vs_y(xval, yval, plot_title, xaxis_title, yaxis_title):
    """Creates a scatter plot of Xvalues vs Yvalues
    
    Arguments:
        xval {list} -- list of the values on the x-axis
        yval {list} -- list of the values on the y-axis
        plot_title {string} -- title of the plot
        xaxis_title {string} -- title of the x-axis
        yaxis_title {string} -- title of the y-axis
        target {directory} -- directory where the image will be saved

    Returns:
        plot {ImageJ-plot} -- an ImageJ plot object
    """

    xArr = array(xval, 'd')
    yArr = array(yval, 'd')
    
    plot = Plot(plot_title, xaxis_title, yaxis_title)
    plot.setColor(Color.BLUE)
    plot.addPoints(xArr,yArr, Plot.CIRCLE)
    plot.setStyle(0, "blue, #a0a0ff, 1, Cirlce")
    
    return plot


def fit_line(xval, yval):
    """Calculates the line fit to Xvalues vs Yvalues
    
    Arguments:
        xval {list} -- list of the values on the x-axis
        yval {list} -- list of the values on the y-axis

    Returns:
        a tuple that contains:
        yfit {list} -- a list that contains the line fit values
        r_squared_value {float} -- the R-squared value
        fit_results {list} -- a list of resulting line fit parameters
    """    

    xArr = array(xval, 'd')
    yArr = array(yval, 'd')

    curve_fitter = CurveFitter(xArr, yArr)
    curve_fitter.doFit(CurveFitter.STRAIGHT_LINE)

    fit_results = curve_fitter.getParams()
    r_squared_value = curve_fitter.getRSquared()

    yfit = []

    for cx in xArr:
        yfit.append(curve_fitter.f(cx))

    return yfit, r_squared_value, fit_results


def add_points_to_plot(plot, xval, yval, style):
    """Adds points to an existing ImageJ-plot object in the specified style
    
    Arguments:
        plot {ImageJ-plot} -- an existing ImageJ-plot object
        xval {list} -- list of the values on the x-axis
        yval {list} -- list of the values on the y-axis
        style {ij.gui.Plot shape} -- specifies the plot style, e.g. Plot.LINE

    Returns:
        nothing
    """

    xArr = array(xval, 'd')
    yArr = array(yval, 'd')

    plot.addPoints(xArr, yArr, style)


def mean(data):
    """Calculates the sample arithmetic mean of the input data.
    
    Arguments:
        data {list} -- a list of values

    Returns:
        mean {float} -- the sample arithmetic mean of the input data
    """

    n = len(data)
    if n < 1:
        raise ValueError('mean requires at least one data point')
    
    mean = sum(data)/float(n)

    return mean


def sum_square_deviation(data):
    """Calculates the sum of square deviations of the input data.
    
    Arguments:
        data {list} -- a list of values

    Returns:
        sum_sq_dev {float} -- the sum of square deviations of the input data
    """

    c = mean(data)
    sum_sq_dev = sum((x-c)**2 for x in data)
    return sum_sq_dev


def stddev(data, ddof=0):
    """Calculates the population standard deviation of the input data
    by default; specify ddof=1 to compute the sample standard deviation.

    Arguments:
    data {list} -- a list of values

    Returns:
        standard_deviation {float} -- the standard deviation of the input data
    """

    n = len(data)
    if n < 2:
        raise ValueError('variance requires at least two data points')
    sum_sq_dev = sum_square_deviation(data)
    pvar = sum_sq_dev/(n-ddof)
    standard_deviation = pvar**0.5

    return standard_deviation


def save_xy_values(target, tabel_name, header_row,  xvalues, yvalues):
    """Saves two lists of values in one csv file, e.g. the 
    X- and Y-values of a plot.

    Arguments:
    data {list} -- a list of values

    Returns:
        standard_deviation {float} -- the standard deviation of the input data
    """

    outCSV = str(target) + "/" + tabel_name + ".csv"
    with open(outCSV, 'wb') as f:
	    writer = csv.writer(f)
	    writer.writerow(header_row)
	    writer.writerows(izip(xvalues, yvalues))
    f.close()
    print "now saving ", outCSV


def save_ij_log_file(filename, target):
    """save the ImageJ-log window as text file
    
    Arguments:
        filename {string} -- the name of the text file
        target {directory} -- directory where the text file will be saved

    Returns:
        nothing
    """
    
    IJ.selectWindow("Log")
    savename = filename + "_Log.txt"
    savepath = str(target) + "/" + savename
    IJ.saveAs("Text", savepath)
    print "now saving", savepath 


# start the process
execution_start_time = time.time()
source = str(source) + "/"
all_source_dirs = list_dirs_containing_filetype(source, filetype)
all_dark_current_values = []
exposure_time_values = []

for source in all_source_dirs:
    IJ.log("now working on " + source)
    allimages = list_all_filenames(source, filetype)
    dark_current = calculate_dark_current(source, allimages[0], allimages[1], gain, read_noise)
    exposure = get_exposure_time(source, allimages[0])
    all_dark_current_values.append(dark_current)
    exposure_time_values.append(exposure[0])

mean_dark_current_value = mean(all_dark_current_values)
dark_current_standard_deviation = stddev(all_dark_current_values)
scatter_plot = plot_x_vs_y(exposure_time_values, all_dark_current_values, "dark_current_vs_exposure", "exposure [s]", "dark_current [e-]")
save_xy_values(target, "dark_current_and_exposure_time", ["time_in_s", "dark_current_in_e-"], sorted(exposure_time_values), sorted(all_dark_current_values))
linear_regression = fit_line(exposure_time_values, all_dark_current_values)
add_points_to_plot(scatter_plot, exposure_time_values, linear_regression[0], Plot.LINE)
scatter_plot.show()
save_current_image_as_PNG(target)
line_fit_r_value = linear_regression[1]

total_execution_time_min = (time.time() - execution_start_time) / 60.0

IJ.log("\\Clear")
IJ.log("##### summary #####")
IJ.log("number of folders processed: " + str(len(all_source_dirs)))
IJ.log("gain value used [e-/greylevel]: " + str(gain))
IJ.log("readnoise value used [e-]: " + str(read_noise))
IJ.log("mean dark_current value [e-]: " + str(mean_dark_current_value))
IJ.log("dark_current standard deviation: " + str(dark_current_standard_deviation))
IJ.log("Line fit R-squared value: " + str(line_fit_r_value))
IJ.log("total time in minutes: " + str(total_execution_time_min))
IJ.log("All done")

save_ij_log_file("dark_current", target)
