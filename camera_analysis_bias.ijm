//@ File (label="Select bias time laps", style="file") raw_image

start_time = getTime();

print("now working on: " + raw_image);
run("Bio-Formats Importer", "open=" + raw_image + " color_mode=Default view=Hyperstack stack_order=XYCZT");

parent_folder = File.getParent(raw_image);
image_title = File.nameWithoutExtension();
outfile_basename = parent_folder + File.separator + image_title;

getDimensions(width, height, channels, slices, frames);

run("Set Measurements...", "mean standard redirect=None decimal=3");
roiManager("reset");
makeRectangle(0, 0, width, height);
roiManager("add");
roiManager("multi measure");
saveAs("Results", outfile_basename + "_Results.txt");

Plot.create("Mean_plot", "x", "Mean1");
Plot.add("Circle", Table.getColumn("Mean1", "Results"));
Plot.setStyle(0, "blue,#a0a0ff,1.0,Circle");
Plot.show();
selectWindow("Mean_plot");
saveAs("PNG", outfile_basename + "_mean.png");

Plot.create("StdDev_plot", "x", "StdDev1");
Plot.add("Circle", Table.getColumn("StdDev1", "Results"));
Plot.setStyle(0, "blue,#a0a0ff,1.0,Circle");
Plot.show();
selectWindow("StdDev_plot");
saveAs("PNG", outfile_basename + "_StdDev.png");

run("Distribution...", "parameter=Mean1 automatic");
selectWindow("Mean1 Distribution");
saveAs("PNG", outfile_basename + "_meanHistogram.png");

run("Distribution...", "parameter=StdDev1 automatic");
selectWindow("StdDev1 Distribution");
saveAs("PNG", outfile_basename + "_StdDevHistogram.png");

run("Close All");

end_time = getTime();
total_time = (end_time - start_time)/ (1000 * 60);
print("total time in min: " + total_time);