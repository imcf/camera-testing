# @File(label="Select bias time laps", style="file") timelaps_image
# @Double(label="enter mean gain value [e-/greylevel]") gain
# @File(label="save results here directory", style="directory") target

# python imports
import csv
from itertools import izip
from math import sqrt
import sys

# Imagej imports
from ij import IJ
from ij import ImagePlus
from ij.plugin import ImageCalculator
from loci.plugins import BF
from loci.plugins.in import ImporterOptions

# ome imports to parse metadata
from loci.formats import ImageReader
from loci.formats import MetadataTools


def get_number_of_frames(path_to_image):
    """Returns the number of frames for a given time laps image from ome metadata
    
    Arguments:
        path_to_image {directory} -- path to the time laps image

    Returns:
        number_of_frames {ome object} -- number of frames. Use int(str(number_of_frames)) to convert
    """

    path_to_image = str(path_to_image)
    reader = ImageReader()
    omeMeta = MetadataTools.createOMEXMLMetadata()
    reader.setMetadataStore(omeMeta)
    reader.setId(path_to_image)
    number_of_frames = omeMeta.getPixelsSizeT(0)
    reader.close()
    
    return  number_of_frames


def open_timelaps_range(timelaps_image, series_number, tbegin, tend):
    """Use Bio-Formats to open a range of time points from a time laps given time laps image
    
    Arguments:
        timelaps_image {directory} -- path to the time laps image
        series_number {integer} -- Bio-Formats series to open
        tbegin {integer} -- first time point
        tend {integer} -- last time point 

    Returns:
        raw_images {array ImagePlus} -- An array of ImagePlus were each element corresponds to a Bio-Formats series
    """

    options = ImporterOptions()
    options.setId(str(timelaps_image))
    options.setColorMode(ImporterOptions.COLOR_MODE_GRAYSCALE)
    options.setTBegin(series_number, tbegin)
    options.setTEnd(series_number, tend)
    options.setTStep(series_number, 1) 
    options.setSeriesOn(series_number, True)
    raw_images = BF.openImagePlus(options)
    # TODO: how do I close the two raw images again?
    
    return raw_images


def calculate_readnoise(imp1, imp2, gain):
    """Calculates the cameras read noise given two input images and the gain

    Arguments:
        imp1 {ImagePlus} -- the first image
        imp2 {ImagePlus} -- the second image
        gain {float} -- value of the camera gain

    Returns:
       readnoise {float} -- value of the camera read noise
    """

    ic = ImageCalculator()
    difference_image = ic.run("Subtract create 32-bit", imp1, imp2)
    difference_image_stats = difference_image.getStatistics()
    stdev_diff_image = difference_image_stats.stdDev
    difference_image.close()

    readnoise = (stdev_diff_image * gain) / sqrt(2.0)

    return readnoise


def save_ij_log_file(filename, target):
    """save the ImageJ-log windows as text file
    
    Arguments:
        filename {string} -- the name of the text file
        target {directory} -- directory where the text file will be saved

    Returns:
        nothing
    """
    
    IJ.selectWindow("Log")
    savename = filename + "_Log.txt"
    savepath = str(target) + "/" + savename
    IJ.saveAs("Text", savepath)
    print "now saving", savepath 


number_of_frames = int(str(get_number_of_frames(timelaps_image)))
if number_of_frames < 2:
    IJ.log("aborting...image contains only one time point")
    sys.exit("image contains only one time point")

first_timepoint = open_timelaps_range(timelaps_image, 0, 1, 1)
second_timepoint = open_timelaps_range(timelaps_image, 0, 2, 2)
readnoise = calculate_readnoise(first_timepoint[0], second_timepoint[0], gain)

IJ.log("\\Clear")
IJ.log("##### summary #####")
IJ.log("used tp1 and tp2 of bias time laps: " + str(timelaps_image))
IJ.log("gain value used [e-/greylevel]: " + str(gain))
IJ.log("readnoise [e-]: " + str(readnoise))
IJ.log("##### All Done #####")

save_ij_log_file("readnoise", target)