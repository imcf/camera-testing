# scripts and tools to evaluate monochrome cameras (for microscopy)
Another tool to use in combination with below scripts is NoiSee

## Camera Bias

## Camera Gain
- place each pair of images at a given exposure time in their own folder
- place all image folders in one folder and give this one to the script, e.g. structured like so:
- camera_gain_all_images/ <-- this is the source folder for the script \
├── camera_gain_exposure_10ms/ \
│   ├── _camera_gain_exposure_10ms_01/ \
│   ├── _camera_gain_exposure_10ms_02/ \
│   ├── camera_gain_exposure_10ms_01.vsi \
│   └── camera_gain_exposure_10ms_02.vsi \
├── camera_gain_exposure_20ms/ \
│   ├── ....

## Camera Read Noise
## Camera Dark Current
- expects the same file and folder organization as camera_analysis_gain.py
- is usually only relevant for exposure times of several seconds

## Camera Dynamic Range
