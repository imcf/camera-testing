# @Double(label="enter mean StdDev per bias image value") stddev_bias
# @Double(label="enter mean gain value [e-/greylevel]") gain
# @Double(label="enter full well capacity from camera manual [e-]") full_well_capacity
# @File(label="save results in this directory", style="directory") target

# python imports
from math import sqrt
from math import log10

# Imagej imports
from ij import IJ


def calculate_dynamic_range(stddev_bias, gain, full_well_capacity):
    """Calculates the camera dynamic range given the StdDev of a bias image, the gain and the full well capacity

    Arguments:
        stddev_bias {float} -- the StdDev of a bias image
        gain {float} -- the gain value of the camera
        full_well_capacity {float} -- the full well capacity of the camera

    Returns:
       calculate_dynamic_range {float} -- the dynamic range of the camera in dB
    """
    rms_noise_level = (stddev_bias * gain) / sqrt(2.0)
    dynamic_range = 20 * log10(full_well_capacity / rms_noise_level)

    return dynamic_range


def save_ij_log_file(filename, target):
    """save the ImageJ-log windows as text file
    
    Arguments:
        filename {string} -- the name of the text file
        target {directory} -- directory where the text file will be saved

    Returns:
        nothing
    """
    
    IJ.selectWindow("Log")
    savename = filename + "_Log.txt"
    savepath = str(target) + "/" + savename
    IJ.saveAs("Text", savepath)
    print "now saving", savepath 


dynamic_range = calculate_dynamic_range(stddev_bias, gain, full_well_capacity)

IJ.log("\\Clear")
IJ.log("##### summary #####")
IJ.log("gain value used [e-/greylevel]: " + str(gain))
IJ.log("full well capacity value used [e-]: " + str(full_well_capacity))
IJ.log("bias StdDev value used: " + str(stddev_bias))
IJ.log("dynamic_range [dB]: " + str(dynamic_range))
IJ.log("##### All Done #####")
save_ij_log_file("dynamic_range", target)